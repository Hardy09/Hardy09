<h1 align="center">Hi <img src="https://camo.githubusercontent.com/e8e7b06ecf583bc040eb60e44eb5b8e0ecc5421320a92929ce21522dbc34c891/68747470733a2f2f6d656469612e67697068792e636f6d2f6d656469612f6876524a434c467a6361737252346961377a2f67697068792e676966" height="29px">, I'm Aniket Jaiswal<br/><small>A full stack developer from India</small></h1>

<p align="center"> <img src="https://komarev.com/ghpvc/?username=hardy09&label=Profile%20views&color=ff4d4d&style=flat" alt="hardy09" /> </p>

<p align="center"> <a href="https://github.com/ryo-ma/github-profile-trophy"><img src="https://github-profile-trophy.vercel.app/?username=hardy09&theme=dracula" alt="hardy09" /></a> </p>

<article class="markdown-body entry-content container-lg f5" itemprop="text">
<h1 dir="auto">
      <span class="AnimatedImagePlayer" data-target="animated-image.player" hidden="">
        <button data-target="animated-image.imageButton" class="AnimatedImagePlayer-images" tabindex="-1" aria-label="Play 68747470733a2f2f6d656469612e67697068792e636f6d2f6d656469612f6876524a434c467a6361737252346961377a2f67697068792e676966"></button>
        <span class="AnimatedImagePlayer-controls" data-target="animated-image.controls">
          <button data-target="animated-image.playButton" class="AnimatedImagePlayer-button" aria-label="Play 68747470733a2f2f6d656469612e67697068792e636f6d2f6d656469612f6876524a434c467a6361737252346961377a2f67697068792e676966">
            <svg aria-hidden="true" focusable="false" class="octicon icon-play" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M4 13.5427V2.45734C4 1.82607 4.69692 1.4435 5.2295 1.78241L13.9394 7.32507C14.4334 7.63943 14.4334 8.36057 13.9394 8.67493L5.2295 14.2176C4.69692 14.5565 4 14.1739 4 13.5427Z">
            </path></svg>
            <svg aria-hidden="true" focusable="false" class="octicon icon-pause" width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
              <rect x="4" y="2" width="3" height="12" rx="1"></rect>
              <rect x="9" y="2" width="3" height="12" rx="1"></rect>
            </svg>
          </button>
          <a data-target="animated-image.openButton" aria-label="Open 68747470733a2f2f6d656469612e67697068792e636f6d2f6d656469612f6876524a434c467a6361737252346961377a2f67697068792e676966 in new window" class="AnimatedImagePlayer-button" href="https://camo.githubusercontent.com/e8e7b06ecf583bc040eb60e44eb5b8e0ecc5421320a92929ce21522dbc34c891/68747470733a2f2f6d656469612e67697068792e636f6d2f6d656469612f6876524a434c467a6361737252346961377a2f67697068792e676966" target="_blank">
            <svg aria-hidden="true" class="octicon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
              <path fill-rule="evenodd" d="M10.604 1h4.146a.25.25 0 01.25.25v4.146a.25.25 0 01-.427.177L13.03 4.03 9.28 7.78a.75.75 0 01-1.06-1.06l3.75-3.75-1.543-1.543A.25.25 0 0110.604 1zM3.75 2A1.75 1.75 0 002 3.75v8.5c0 .966.784 1.75 1.75 1.75h8.5A1.75 1.75 0 0014 12.25v-3.5a.75.75 0 00-1.5 0v3.5a.25.25 0 01-.25.25h-8.5a.25.25 0 01-.25-.25v-8.5a.25.25 0 01.25-.25h3.5a.75.75 0 000-1.5h-3.5z"></path>
            </svg>
          </a>
        </span>
      </span></animated-image></h1>
<h2 dir="auto"><a id="user-content--about-me" class="anchor" aria-hidden="true" href="#-about-me"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a><g-emoji class="g-emoji" alias="rocket" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/1f680.png">🚀</g-emoji> About Me</h2>
<p dir="auto"><g-emoji class="g-emoji" alias="mortar_board" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/1f393.png">🎓</g-emoji> A <strong> Full Stack Developer </strong>  with an illustrious history in the information technology and services sector, possessing a robust skill set in both backend and frontend development. I have obtained a Bachelor of Technology degree with a focus on Computer Science and Engineering..<br/>

<br>- 🔭 I’m currently working on **<a target='_blank' href="https://koinpro.io/portal/website/web"><b>Crypto Exchange<b/><a/>**</a><br/>

<br>- 🌱 I’m currently learning **AWS**<br/>

<br>- 💬 Ask me about **Mongodb, Nodejs, Flutter, Angular, Express**<br/>

<br>- 📫 How to reach me **aniketjaiswal094@gmail.com**<br/>

<strong>Connect with me:</strong>

<a href="https://www.linkedin.com/in/aniket-jaiswal" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/linked-in-alt.svg" alt="https://www.linkedin.com/in/aniket-jaiswal" height="30" width="40" /></a>

</p>

<p dir="auto"><g-emoji class="g-emoji" alias="guitar" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/1f3b8.png">🎸</g-emoji> Aside from the realm of technology, I relish the challenge of acquiring new competencies and expanding my skill set.</p>

<h2 dir="auto">
<svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg>
<i class="g-emoji" alias="hammer_and_wrench" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/1f6e0.png">🛠️</i> Skills
</h2>

<h3 dir="auto"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg>Languages</h3>
<p dir="auto">
<img src="https://raw.githubusercontent.com/np4652/np4652/7f908dd1d638146c26170d8cd4c9268b1abc160c/.github/Images/typescript.svg" alt="typescript"/>
<img src="https://raw.githubusercontent.com/np4652/np4652/7f908dd1d638146c26170d8cd4c9268b1abc160c/.github/Images/javascript.svg" alt="javascript"/>
<img src="https://gitlab.com/Hardy09/Hardy09/-/raw/main/.github/Images/dart.png" alt="dart"/>

</p>
<!--<img src="" alt="python" />-->


<h3 dir="auto">
<svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true">
    <path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path>
</svg>
    Front-End Development
</h3>

<p dir="auto">
<img src="https://gitlab.com/Hardy09/Hardy09/-/raw/main/.github/Images/angular.svg" alt="angular"/>
<img src="https://raw.githubusercontent.com/np4652/np4652/a43733d5a43707400b3a34bbdebc1266a35b52cb/.github/Images/MUI.svg" alt="material-ui" data-canonical-src="https://img.shields.io/badge/Material_UI-0081CB?style=for-the-badge&amp;logo=mui&amp;logoColor=white" style="max-width: 100%;"/>
<img src="https://raw.githubusercontent.com/np4652/np4652/445c009e09f434659b9aeee5fb4a6d4ac1cdbb5f/.github/Images/PWA.svg" alt="pwa" data-canonical-src="https://img.shields.io/badge/Progressive_Web_App-4285F4?style=for-the-badge&amp;logo=googlechrome&amp;logoColor=white" style="max-width: 100%;"/>
<img src="https://raw.githubusercontent.com/np4652/np4652/445c009e09f434659b9aeee5fb4a6d4ac1cdbb5f/.github/Images/HTML.svg" alt="html" data-canonical-src="https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&amp;logo=html5&amp;logoColor=white" style="max-width: 100%;"/>
<img src="https://raw.githubusercontent.com/np4652/np4652/d2d40fcf863cb6006fe91aa20e1a1c2b8de307ff/.github/Images/css.svg" alt="css" data-canonical-src="https://img.shields.io/badge/CSS3-1572B6?style=for-the-badge&amp;logo=css3&amp;logoColor=white" style="max-width: 100%;"/>
<img src="https://raw.githubusercontent.com/np4652/np4652/445c009e09f434659b9aeee5fb4a6d4ac1cdbb5f/.github/Images/Sass.svg" alt="sass" data-canonical-src="https://img.shields.io/badge/SASS-CC6699?style=for-the-badge&amp;logo=sass&amp;logoColor=white" style="max-width: 100%;"/>
<img src="https://raw.githubusercontent.com/np4652/np4652/d2d40fcf863cb6006fe91aa20e1a1c2b8de307ff/.github/Images/Bootstrap.svg" alt="bootstrap" data-canonical-src="https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&amp;logo=bootstrap&amp;logoColor=white"/>
<img src="https://gitlab.com/Hardy09/Hardy09/-/raw/main/.github/Images/flutter.svg" alt="bootstrap" data-canonical-src="https://gitlab.com/Hardy09/Hardy09/-/raw/main/.github/Images/flutter.svg"/>
</p>

<h3 dir="auto"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg>Back-End-Development
<p dir="auto">
<img src="https://gitlab.com/Hardy09/Hardy09/-/raw/main/.github/Images/Express%20js.png" alt="express"/>
<img src="https://gitlab.com/Hardy09/Hardy09/-/raw/main/.github/Images/Mongo%20db.png" alt="mongodb"/>
<img src="https://gitlab.com/Hardy09/Hardy09/-/raw/main/.github/Images/Node%20js.png" alt="nodejs"/>
<img src="https://gitlab.com/Hardy09/Hardy09/-/raw/main/.github/Images/Web%203.png" alt="web3"/>
</p>

<h2 dir="auto"><a id="user-content-️-workspace-setup" class="anchor" aria-hidden="true" href="#️-workspace-setup"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a><g-emoji class="g-emoji" alias="desktop_computer" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/1f5a5.png">🖥️</g-emoji> IDEs</h2>
<p>
<img src="https://gitlab.com/Hardy09/Hardy09/-/raw/main/.github/Images/Pycharm.svg" alt="pycharm">
<img src="https://raw.githubusercontent.com/np4652/np4652/17221fbbb448f5b343513e50c784de23f288d7b6/.github/Images/VSC.svg" alt="vs-code">
<img src="https://gitlab.com/Hardy09/Hardy09/-/raw/main/.github/Images/Android.svg" alt="android">
<img src="https://gitlab.com/Hardy09/Hardy09/-/raw/main/.github/Images/anaconda.svg" alt="anaconda">
</p>

<h2>&nbsp;</h2>
<p>
<img src="https://github-readme-stats.vercel.app/api/top-langs?username=hardy09&show_icons=true&locale=en&layout=compact&theme=dracula" alt="hardy09" style="width:46%"/>
<img align="right" src="https://github-readme-stats.vercel.app/api?username=hardy09&show_icons=true&locale=en&theme=dracula" alt="hardy09" style="width:52%"/>
</p>
<p><img align="center" src="https://github-readme-streak-stats.herokuapp.com/?user=hardy09&theme=dracula" alt="hardy09" style="width:100%"/></p>


